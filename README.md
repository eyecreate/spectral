# Spectral

[![Pipeline Status](https://gitlab.com/b0/spectral/badges/master/pipeline.svg)](https://gitlab.com/b0/spectral/commits/master)
[![Build Status](https://ci.appveyor.com/api/projects/status/qxxwgtan9fdookv8?svg=true)](https://ci.appveyor.com/project/BlackHat/spectral)
[![Build Status](https://travis-ci.org/encombhat/spectral.svg?branch=master)](https://travis-ci.org/encombhat/spectral)

<a href='https://flathub.org/apps/details/org.eu.encom.spectral'><img width='240' alt='Get it on Flathub' src='https://flathub.org/assets/badges/flathub-badge-i-en.png'/></a>

> "Nobody can be told what the matrix is, you have to see it for yourself. "

Spectral is a glossy cross-platform client for Matrix, the decentralized communication protocol for instant messaging.

## Document

There is a separate document for Spectral, including installing, compiling, etc.

It is at [Spectral Doc](https://doc.spectral.encom.eu.org/)

## Contact

You can reach the maintainer at #spectral:matrix.org, if you are already on Matrix.

Also, you can file an issue at this project if anything goes wrong.

## Acknowledgement

This program uses libqmatrixclient library and some C++ models from Quaternion. 

[Quaternion](https://github.com/QMatrixClient/Quaternion)

[libqmatrixclient](https://github.com/QMatrixClient/libqmatrixclient)

## License

![GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png)

This program is licensed undedr GNU General Public License, Version 3. 